import java.util.Scanner;

public class oxgit {
	public static int row,col;
	public static Scanner kb = new Scanner(System.in);
	public static int round =0;
	public static char turn ;

	public static void showWelcome() {
		System.out.println("Welcome To OX Game");
	}
	
	public static char Table [][]  = {{' ', '|', ' ', '|',' ' },
			                          {'-', '+', '-', '+','-' },
			                          {' ', '|', ' ', '|',' ' },
			                          {'-', '+', '-', '+','-' },
			                          {' ', '|', ' ', '|',' ' }
			                          };
	
		
	public static void showTable(char  Teble[][]) {
		for(int i = 0 ;i < Table.length ;i++ ) {
			for (int j = 0; j < Table[i].length; j++) {
				System.out.print(Table[i][j] + " ");
			}
			System.out.println();
		}				
	}		

	public static void showTurn() {
		 round =  round + 1;
		if ( round % 2 == 0) {
			System.out.println();
			System.out.println("X Turn");
			turn = 'X';
		} else {
			System.out.println();
			System.out.println("O Turn");
			turn = 'O';
			}
		}
	

	public static void Input() {
		while (true) {
			System.out.print("Please in put row, col :");
			row = kb.nextInt()-1;
			col = kb.nextInt() - 1;
			if(Table[row][col] != 'X' && Table[row][col] != 'O') {
				Table[row][col] = turn;
				break;
			}else {
				System.out.println("This cell is occupied! Choose another one!");
				}
			}	
		}

	public static boolean rowcheck() {
		for (int i = 0; i < 3; i++) {
			if (Table[i][0] == Table[i][1] && Table[i][1] == Table[i][2])
				return true;
		}
		return false;
	}

	public static boolean colcheck() {
		for (int i = 0; i < 3; i++) {
			if (Table[0][i] == Table[1][i] && Table[1][i] == Table[2][i])
				return true;
		}
		return false;
	}

	public static boolean checkdiagonal() {
		if (Table[0][0] == Table[1][1] && Table[0][0] == Table[2][2] )
			return true; // This is "\" check
		if (Table[0][2] == Table[1][1] && Table[0][2] == Table[2][0])
			return true; // This is "/" check
		return false;
	}
	public static boolean Gameover() {
		if (rowcheck() || colcheck() || checkdiagonal()) {
			showTurn();
			if (round % 2 == 1) {
				System.out.println("X Win!\n");
			} else {
				System.out.println("O Win!\n");
			}
			return true;  
		}
		if (round == 9) {
			showTable(Table);
			System.out.println("Draw!\n");
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		showWelcome();
		while (true) {
			showTable(Table);
			showTurn();
			Input(); 
			if (Gameover()) {
				break;
			}
		}
	}

}
