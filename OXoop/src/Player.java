
public class Player {

	private char name;
	private int win;
	private int loser;
	private int draw;

	public Player(char name) {
		this.name = name;

	}

	public char getName() {
		return name;
	}

	public String tostring() {
		return "Player{" + "name=" + name + '}';
	}
	public int getWin() {
		return win;
	}
	public int getLoser() {
		return loser;
	}
	public int getDraw() {
		return draw;
	}
	
	public void win() {
		this.win++;
	}
	public void loser() {
		this.loser++;
	}
	public void draw() {
		this.draw++;
	}

}
